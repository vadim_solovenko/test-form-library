import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import View from "./index";

export const actions = {
  onClick: action("onClick"),
};

storiesOf("View", module).add("default", () => <View />);
