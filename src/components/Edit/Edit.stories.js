import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import Edit from "./index";

export const actions = {
  onClick: action("onClick"),
};

storiesOf("Edit", module).add("default", () => <Edit />);
