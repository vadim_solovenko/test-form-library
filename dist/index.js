"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "View", {
  enumerable: true,
  get: function get() {
    return _Icons.default;
  }
});
Object.defineProperty(exports, "Edit", {
  enumerable: true,
  get: function get() {
    return _UI.default;
  }
});

var _Icons = _interopRequireDefault(require("./Icons"));

var _UI = _interopRequireDefault(require("./UI"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }